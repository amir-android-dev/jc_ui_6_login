package com.amir.jc_ui_6_login

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_6_login.ui.theme.AppGray
import com.amir.jc_ui_6_login.ui.theme.AppRed
import com.amir.jc_ui_6_login.ui.theme.Jc_ui_6_loginTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Jc_ui_6_loginTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainView()
                }
            }
        }
    }
}

@Composable
private fun MainView() {
    var username by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }

    Box(modifier = Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(AppRed),
        ) {
            //the first box behind
            Row(
                Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                MyIconButton(R.drawable.facebook)
                MyIconButton(R.drawable.instagram)
                MyIconButton(R.drawable.twitter)
            }
        }
        //the second box on the first box
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(700.dp)
                .clip(RoundedCornerShape(0.dp, 0.dp, 60.dp, 60.dp))
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
            ) {
                ForgroundBoxBody(username, password)
            }
        }
    }
}

@Composable
private fun MyIconButton(img: Int) {
    IconButton(onClick = { /*TODO*/ }) {
        Box(modifier = Modifier.clip(RoundedCornerShape(50))) {
            Image(
                painter = painterResource(id = img),
                contentDescription = "",
                Modifier
                    .size(30.dp)
                    .background(Color.White)
                    .padding(5.dp)
                    .align(Alignment.Center)
            )
        }
    }
}

@Composable
@OptIn(ExperimentalMaterial3Api::class)
private fun ForgroundBoxBody(username: String, password: String) {
    var username1 = username
    var password1 = password
    Column(
        Modifier
            .fillMaxWidth()
            .padding(25.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        //img
        Image(
            painter = painterResource(id = R.drawable.teacher),
            contentDescription = "programmer",
            Modifier.fillMaxWidth()
        )
        //title
        Spacer(modifier = Modifier.height(20.dp))
        Text(text = "Login", fontSize = 28.sp)
        Spacer(modifier = Modifier.height(8.dp))
        //text field
        OutlinedTextField(
            value = username1,
            onValueChange = { username1 = it },
            Modifier.fillMaxWidth(),
            label = {
                Text(text = "Username")
            },
            singleLine = true,
            shape = RoundedCornerShape(50)
        )
        //password
        OutlinedTextField(
            value = password1,
            onValueChange = { password1 = it },
            Modifier.fillMaxWidth(),
            label = {
                Text(text = "Password")
            },
            singleLine = true,
            shape = RoundedCornerShape(50)
        )
        Spacer(modifier = Modifier.height(20.dp))
        //btn
        Button(
            onClick = { /*TODO*/ },
            Modifier
                .fillMaxWidth()
                .height(50.dp)
                .clip(RoundedCornerShape(50)),
            colors = ButtonDefaults.buttonColors(
                contentColor = Color.White,
                containerColor = AppRed
            )
        ) {
            Text(text = "Login")
        }

        //
        Row(
            Modifier
                .fillMaxWidth()
                .padding(25.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,

            ) {
            TextButton(onClick = { /*TODO*/ }) {
                Text(text = "Forget Password", color = AppGray)
            }
            TextButton(onClick = { /*TODO*/ }) {
                Text(text = "Register", color = AppGray)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Jc_ui_6_loginTheme {
        MainView()
    }
}